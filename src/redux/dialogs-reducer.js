const SEND_NEW_MESSAGE = 'samurai-network/dialogs/ADD-NEW-MESSAGE';

let initialState = {
    dialogs: [
        {id: 1, name: 'Ira'},
        {id: 2, name: 'Masha'},
        {id: 3, name: 'Victoria'},
        {id: 4, name: 'Dasha'},
    ],

    messages: [
        {id: 1, text: "Hi"},
        {id: 2, text: "Hi are you?"},
        {id: 3, text: "I is normal"},
        {id: 4, text: "And you?"},
        {id: 5, text: "I normal too"},
        {id: 6, text: "How do you do?"},
        {id: 7, text: "I playing to pc games!"},
        {id: 8, text: "And you??"},
    ]
};

const dialogsReducer = (state = initialState, action) => {
    let stateCopy;

    switch (action.type) {
        case SEND_NEW_MESSAGE:
            let newMessageBody = {
                id: state.messages.length + 1,
                text: action.newMessageText.trim()
            };

            if (newMessageBody.text) {
                stateCopy = {
                    ...state,
                    messages: [...state.messages, newMessageBody],
                }
            } else stateCopy = state;

            return stateCopy;

        default:
            return state;
    }
};

export const addMessageActionCreator = newMessageText => ({ type: SEND_NEW_MESSAGE, newMessageText });

export default dialogsReducer;