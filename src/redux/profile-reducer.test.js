import React from 'react';
import profileReducer, {addPostActionCreator, deletePost} from "./profile-reducer";

let state = {
    posts: [
        {id: 1, message: "Hi, I am man :-)", countLike: 15},
        {id: 2, message: "Hi, I am girl :-(", countLike: 10},
    ]
};

test('length of posts should be incremented', () => {
    //1. test data
    let action = addPostActionCreator('Hello');

    //2. Action
    let newState = profileReducer(state, action);

    //3. Expectation
    expect(newState.posts.length).toBe(3);
});

test('message of new posts should be correct', () => {
    //1. test data
    let action = addPostActionCreator('Hello');

    //2. Action
    let newState = profileReducer(state, action);

    //3. Expectation
    expect(newState.posts[2].message).toBe('Hello');
});

test('count likes of new posts should be correct', () => {
    //1. test data
    let action = addPostActionCreator('Hello');

    //2. Action
    let newState = profileReducer(state, action);

    //3. Expectation
    expect(newState.posts[2].countLike).toBe(0);
});

test('after deleting length of posts should`t be decremented if id is incorrect', () => {
    //1. test data
    let action = deletePost(45);

    //2. Action
    let newState = profileReducer(state, action);

    //3. Expectation
    expect(newState.posts.length).toBe(2);
});
