import FriendAvatar1 from '../assets/images/friends-ava-1.jpg';
import FriendAvatar2 from '../assets/images/friends-ava-2.jpg';
import FriendAvatar3 from '../assets/images/friends-ava-3.jpg';

import specificationsSidebarReducer from "./sidebar-reducer";
import dialogsReducer from "./dialogs-reducer";
import profileReducer from "./profile-reducer";

let store = {
    _state: {
        profilePage: {
            posts: [
                {id: 1, message: "Hi, I am man :-)", countLike: 15},
                {id: 2, message: "Hi, I am girl :-(", countLike: 10},
            ],

            newPostText: ''
        },

        messagesPage: {
            dialogs: [
                {id: 1, name: 'Ira'},
                {id: 2, name: 'Masha'},
                {id: 3, name: 'Victoria'},
                {id: 4, name: 'Dasha'},
            ],

            messages: [
                {id: 1, text: "Hi"},
                {id: 2, text: "Hi are you?"},
                {id: 3, text: "I is normal"},
                {id: 4, text: "And you?"},
                {id: 5, text: "I normal too"},
                {id: 6, text: "How do you do?"},
                {id: 7, text: "I playing to pc games!"},
                {id: 8, text: "And you??"},
            ],

            newMessageBody: ''
        },

        specifications: {
            sidebar: {
                friends: [
                    {id: 1, img: FriendAvatar1, altImg: 'Avatar of a profile 1', profileName: 'Lolita'},
                    {id: 2, img: FriendAvatar2, altImg: 'Avatar of a profile 2', profileName: 'Veronika'},
                    {id: 3, img: FriendAvatar3, altImg: 'Avatar of a profile 3', profileName: 'Sasha'},
                ]
            }
        }
    },
    setState(state) {
        if (state) {
            this._state = state;
        }
    },
    getState() {
        return this._state;
    },

    _render(observer) {
        if (!observer) {
            console.log('Render is not defined');
        }
    },
    setRender(method) {
        if (method) {
            this._render = method;
        }
    },
    getRender() {
        this._render();
    },

    _newPost() {
        return {
            id: this._state.profilePage.posts.length + 1,
            message: this._state.profilePage.newPostText,
            countLike: 0
        }
    },
    getPostNew() {
        return this._newPost();
    },

    _newMessage() {
        return {
            id: this._state.messagesPage.messages.length + 1,
            text: this._state.messagesPage.newMessageBody
        }
    },
    getMessageNew() {
      return this._newMessage();
    },

    dispatch(action) {
        this._state.messagesPage = dialogsReducer(this._state.messagesPage, {content: action, getNewMessageBody: this.getMessageNew.bind(this)});
        this._state.profilePage = profileReducer(this._state.profilePage, {content: action, getNewPost: this.getPostNew.bind(this)});
        this._state.specifications.sidebar = specificationsSidebarReducer(this._state.specifications.sidebar, action);

        this.getRender();
    }
};
window.store = store;

export default store;