import {profileAPI} from "../api/api";
import {stopSubmit} from "redux-form";

const ADD_POST = 'samurai-network/profile/ADD-POST';
const SET_USER_PROFILE = 'samurai-network/profile/SET-USER-PROFILE';
const SET_STATUS = 'samurai-network/profile/SET-STATUS';
const DELETE_POST = 'samurai-network/profile/DELETE-POST';

const SAVE_PROFILE_PHOTO = 'samurai-network/profile/SAVE-PROFILE-PHOTO';
const TOGGLE_APPROVE_PROFILE_INFO = 'samurai-network/profile/TOGGLE-APPROVE-PROFILE-INFO';

let initialState = {
    profile: null,
    status: "",
    isUpdateProfile: false,

    posts: [
        {id: 1, message: "Hi, I am man :-)", countLike: 15},
        {id: 2, message: "Hi, I am girl :-(", countLike: 10},
    ]
};

const profileReducer = (state = initialState, action) => {
    let copyState;

    switch (action.type) {
        case ADD_POST:
            let newPost = {
                id: state.posts.length + 1,
                message: action.newPostText.trim(),
                countLike: 0
            };

            if (newPost.message) {
                copyState = {
                    ...state,
                    posts: [...state.posts, newPost],
                };
            } else copyState = state;

            return copyState;

        case SET_USER_PROFILE:
            return {...state, profile: action.profile};

        case SET_STATUS:
            return {...state, status: action.status };

        case DELETE_POST:
            return {
                ...state,
                posts: state.posts.filter( p => p.id !== action.postId)
            };

        case TOGGLE_APPROVE_PROFILE_INFO:
            return {
                ...state,
                isUpdateProfile: action.status
            };

        default:
            return state;
    }
};

export const setUserProfile = profile => ({ type: SET_USER_PROFILE, profile });
export const setStatus = newStatusText => ({ type: SET_STATUS, status: newStatusText });

export const addPostActionCreator = newPostText => ({ type: ADD_POST, newPostText });
export const deletePost = postId => ({ type: DELETE_POST, postId });

export const saveProfilePhotoSuccess = photos => ({ type: SAVE_PROFILE_PHOTO, photos });
export const toggleApproveSaveProfile = status => ({ type: TOGGLE_APPROVE_PROFILE_INFO, status });


export const getStatus = userId => async dispatch => {
    let responseApiStatusText = await profileAPI.getStatus(userId);

    dispatch(setStatus(responseApiStatusText));
};

export const updateStatus = newStatusText => async dispatch => {
    let responseAPI = await updateStatus(newStatusText);

    if (responseAPI.resultCode === 0) {
        dispatch(setStatus(newStatusText));
    }
};

export const saveProfilePhoto = file => async dispatch => {
    let responseAPI = await profileAPI.saveProfilePhoto(file);

    if (responseAPI.resultCode === 0) {
        dispatch(saveProfilePhotoSuccess(responseAPI.data.photos));
    }
};

export const getProfileInfo = userId => async dispatch => {
    let responseAPI = await profileAPI.getProfile(userId);

    dispatch(setUserProfile(responseAPI.data));
};

export const saveProfileData = data => async (dispatch, getState) => {
    let responseAPI = await profileAPI.saveProfileData(data);
    const userId = getState().auth.userId;

    if (responseAPI.resultCode === 0) {
        dispatch(getProfileInfo(userId));
        dispatch(toggleApproveSaveProfile(true));

        setTimeout(() => {
            dispatch(toggleApproveSaveProfile(false));
        }, 1500);
    } else {
        let errorFieldName = responseAPI.messages[0].split('->')[1].split(')')[0].trim().toLowerCase();

        dispatch( stopSubmit('editProfile', {contacts: {[errorFieldName]: responseAPI.messages[0]}}) );
    }
};

export default profileReducer;