import {followAPI, usersAPI} from "../api/api";
import {updateObjectInArray} from "../utils/object-helpers";

const FOLLOW = 'samurai-network/users/FOLLOW';
const UNFOLLOW = 'samurai-network/users/UNFOLLOW';
const SET_USERS = 'samurai-network/users/SET_USERS';
const SET_TOTAL_COUNT_USERS = 'samurai-network/users/SET_TOTAL_COUNT_USERS';
const TOGGLE_IS_FETCHING = 'samurai-network/users/TOGGLE_IS_FETCHING';
const TOGGLE_IS_FOLLOWING_PROGRESS = 'samurai-network/users/TOGGLE-IS-FOLLOWING-PROGRESS';

let initialState = {
    users: [],
    pageSize: 5,
    totalUsersCount: 0,
    isFetching: false,
    currentPage: 1,
    followingInProgress: []
};

const usersReducer = (state = initialState, action) => {
    switch (action.type) {
        case FOLLOW:
            return {
                ...state,
                users: updateObjectInArray(state.users, action.userId, 'id', {followed: true})
            };

        case UNFOLLOW:
            return {
                ...state,
                users: updateObjectInArray(state.users, action.userId, 'id', {followed: false})
            };

        case SET_USERS:
            return {...state, users: [...state.users, ...action.users]};

        case SET_TOTAL_COUNT_USERS:
            return {...state, totalUsersCount: action.count};

        case TOGGLE_IS_FETCHING:
            return {...state, isFetching: action.isFetching};

        case TOGGLE_IS_FOLLOWING_PROGRESS:
            return {
                ...state,
                followingInProgress: action.isFetching
                    ? [...state.followingInProgress, action.userId]
                    : [...state.followingInProgress.filter(id => id !== action.userId)]
            };

        default:
            return state;
    }
};

export const followSuccess = userId => ({ type: FOLLOW, userId });
export const unfollowSuccess = userId => ({ type: UNFOLLOW, userId });

export const setUsers = users => ({ type: SET_USERS, users });
export const setUsersTotalCount = totalUsersCount => ({ type: SET_TOTAL_COUNT_USERS, count: totalUsersCount });

export const toggleIsFetching = isFetching => ({ type: TOGGLE_IS_FETCHING, isFetching });
export const toggleFollowingProgress = (isFetching, userId) => ({ type: TOGGLE_IS_FOLLOWING_PROGRESS, isFetching, userId});


export const getUsers = (currentPage, pageSize) => async (dispatch) => {
    dispatch(toggleIsFetching(true));

    let responseAPI = await usersAPI.getUsers(currentPage, pageSize);

    dispatch(toggleIsFetching(false));

    dispatch(setUsers(responseAPI.items));
    dispatch(setUsersTotalCount(responseAPI.totalCount));
};

export const toggleUserFollow = async (dispatch, userId, apiMethod, actionCreator) => {
    dispatch(toggleFollowingProgress(true, userId));

    let responseAPI = await apiMethod(userId);

    if (responseAPI.resultCode === 0) dispatch(actionCreator(userId));

    dispatch(toggleFollowingProgress(false, userId));
};

export const follow = userId => (dispatch) => {
    toggleUserFollow(dispatch, userId, followAPI.follow.bind(followAPI), followSuccess);
};

export const unfollow = userId => (dispatch) => {
    toggleUserFollow(dispatch, userId, followAPI.unfollow.bind(followAPI), unfollowSuccess);
};

export default usersReducer;