export const getStateUsers = (state) => state.usersPage.users;
//Example the Reselector
// export const getStateUsers = createSelector(getUsersSelector, (users) => {
//     return users.filter( u => true);
// });

export const getPageSize = (state) => state.usersPage.pageSize;
export const getTotalUsersCount = (state) => state.usersPage.totalUsersCount;
export const getCurrentPage = (state) => state.usersPage.currentPage;
export const getStatusFetching = (state) => state.usersPage.isFetching;
export const getFollowingProgress = (state) => state.usersPage.followingInProgress;
