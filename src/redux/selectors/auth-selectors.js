export const getAuthInfo = (state) => state.auth;
export const getProfileId = (state) => state.auth.userId;
export const getFetchingStatus = (state) => state.auth.isFetching;
export const getAuthStatus = (state) => state.auth.isAuth;
export const getAuthLogin = (state) => state.auth.email;
export const getCaptchaImage = (state) => state.auth.urlCaptcha;