export const getMessages = state => state.messagesPage.messages;
export const getNewMessageBody = state => state.messagesPage.newMessageBody;
export const getDialogs = state => state.messagesPage.dialogs;