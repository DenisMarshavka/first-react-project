export const getProfile = (state) => state.profilePage.profile;
export const getProfileStatus = (state) => state.profilePage.status;
export const getIsUpdateProfile = (state) => state.profilePage.isUpdateProfile;