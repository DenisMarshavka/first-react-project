import FriendAvatar1 from "../assets/images/friends-ava-1.jpg";
import FriendAvatar2 from "../assets/images/friends-ava-2.jpg";
import FriendAvatar3 from "../assets/images/friends-ava-3.jpg";

const ADD_FRIENDS = 'samurai-network/sidebar/ADD-FRIENDS';

let initialState = {
    friends: [
        {id: 1, img: FriendAvatar1, altImg: 'Avatar of a profile 1', profileName: 'Lolita'},
        {id: 2, img: FriendAvatar2, altImg: 'Avatar of a profile 2', profileName: 'Veronika'},
        {id: 3, img: FriendAvatar3, altImg: 'Avatar of a profile 3', profileName: 'Sasha'},
    ]
};

const sidebarReducer = (state = initialState, action) => {
    switch (state.type) {
        case ADD_FRIENDS:
            if (true) {}

            return state;

        default:
            return state;
    }
};

export default sidebarReducer;