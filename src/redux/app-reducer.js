import {getAuth} from "./auth-reducer"

const INITIALIZED_SUCCESS = 'samurai-network/app/INITIALIZED-SUCCESS';

let initialState = {
    initialized: false
};

const appReducer = (state = initialState, action) => {
    switch (action.type) {
        case INITIALIZED_SUCCESS:
            return {
                ...state,
                initialized: true
            };

        default:
            return state;
    }
};

export const initializedSuccess = () => ({ type: INITIALIZED_SUCCESS });

export const initializeApp = () => dispatch => {
    let dispatchResult = dispatch(getAuth());

    Promise.all([dispatchResult])
        .then(() => {
            dispatch(initializedSuccess());
        });
};

export default appReducer;
