import {authAPI, securityAPI} from "../api/api";
import {stopSubmit} from "redux-form";

const SET_USER_DATA = 'samurai-network/auth/SET_USER-DATA';
const CHANGE_TOGGLE_IS_FETCHING = 'samurai-network/auth/CHANGE-TOGGLE-IS-FETCHING';
const SET_CAPTCHA = 'samurai-network/auth/SET-CAPTCHA';

let initialState = {
    urlCaptcha: null,
    userId: null,
    login: null,
    email: null,
    isFetching: false,
    isAuth: false
};

const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_USER_DATA:
            return {
                ...state,
                ...action.payload,
                isFetching: false
            };

        case CHANGE_TOGGLE_IS_FETCHING:
            return {
                ...state,
                isFetching: action.isFetching
            };

        case SET_CAPTCHA:
            return {
                ...state,
                urlCaptcha: action.urlCaptcha
            };

        default:
            return state;
    }
};

export const setAuthUserData = (userId, email, login, isAuth) => ({ type: SET_USER_DATA, payload: {userId, email, login, isAuth} });
export const changeToggleIsFetching = isFetching => ({ type: CHANGE_TOGGLE_IS_FETCHING, isFetching });
export const setSecurityCaptcha = urlCaptcha => ({ type: SET_CAPTCHA, urlCaptcha });

export const getSecurityCaptcha = () => async dispatch => {
    let responseAPI = await securityAPI.getCaptcha();

    if (responseAPI.url) dispatch(setSecurityCaptcha(responseAPI.url));
};

export const getAuth = () => async dispatch => {
    dispatch(changeToggleIsFetching(true));

    let responseAPI = await authAPI.checkAuth();

    dispatch(changeToggleIsFetching(false));

    if (responseAPI.resultCode === 0) {
        let {id, login, email} = responseAPI.data;

        return dispatch(setAuthUserData(id, login, email, true));
    }
};

export let getLogin = (email, password, rememberMe, captcha) => async dispatch => {
    let responseAPI = await authAPI.login(email, password, rememberMe, captcha);

    if (responseAPI.resultCode === 0) {
        dispatch(getAuth());
    } else {
        if (responseAPI.resultCode === 10) {
            dispatch( getSecurityCaptcha() );
        }

        let messageError = (responseAPI.messages.length) ? responseAPI.messages[0] : 'Fields haves error...';
        dispatch( stopSubmit('login', {_error: `Common error! ${messageError}`}) );
    }
};

export const logout = () => async dispatch => {
    let responseAPI = await authAPI.logOut();

    if (responseAPI.resultCode === 0) {
        dispatch(setAuthUserData(null, null, null, false));
    }
};

export default authReducer;