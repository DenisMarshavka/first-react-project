import React from 'react';

import ProfileDescription from "./ProfileDescription/ProfileDescription";
import MyPostsContainer from "./MyPostst/MyPostsContainer";
import Preloader from "../../specifications/Preloader/Preloader";

const Profile = React.memo(props => {
    if (!props.profile) return <Preloader />;

    return (
        <main>
            <ProfileDescription
                isOwner={props.isOwner} profile={props.profile}
                status={props.status} updateStatus={props.updateStatus}
                myProfileId={props.myProfileId} saveProfilePhoto={props.saveProfilePhoto}
                saveProfile={props.saveProfile} isUpdateProfile={props.isUpdateProfile}
            />

            <MyPostsContainer />
        </main>
    );
});

export default Profile;