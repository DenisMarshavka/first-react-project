import React from 'react';
import {connect} from "react-redux";

import Profile from "./Profile";
import {
    getProfileInfo,
    getStatus,
    saveProfileData,
    saveProfilePhoto,
    updateStatus
} from "../../redux/profile-reducer";
import {withRouter} from "react-router-dom";
import withAuthRedirectComponent from "../../hoc/withAuthRedirect";
import {compose} from "redux";
import {getIsUpdateProfile, getProfile, getProfileStatus} from "../../redux/selectors/profile-selectors";
import {getAuthInfo, getProfileId} from "../../redux/selectors/auth-selectors";

class ProfileContainer extends React.Component {
    refreshProfile() {
        let userId = this.props.match.params.userId;

        if (!userId) userId = (this.props.authInfo.userId) ? this.props.authInfo.userId : this.props.history.push("/login");

        this.props.getProfileInfo(userId);
        this.props.getStatus(userId);
    }

    componentDidMount() {
        this.refreshProfile();
    }

    componentDidUpdate(prevProps) {
        if (this.props.match.params.userId !== prevProps.match.params.userId) {
            this.refreshProfile();
        }
    }

    render() {
        return (
            <Profile {...this.props} isOwner={!this.props.match.params.userId}
                     profile={this.props.profile} status={this.props.status}
                     updateStatus={this.props.updateStatus} saveProfilePhoto={this.props.saveProfilePhoto}
                     saveProfile={this.props.saveProfileData} isUpdateProfile={this.props.isUpdateProfile}
            />
        );
    }
}

let mapStateToProps = state => ({
    profile: getProfile(state),
    myProfileId: getProfileId(state),
    status: getProfileStatus(state),
    authInfo: getAuthInfo(state),
    isUpdateProfile: getIsUpdateProfile(state),
});

export default compose(
    connect(mapStateToProps, {getProfileInfo, getStatus, updateStatus, saveProfilePhoto, saveProfileData}),
    withRouter,
    withAuthRedirectComponent
)(ProfileContainer);