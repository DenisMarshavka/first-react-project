import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faFacebookF, faGithub, faInstagram, faTwitter, faVk, faYoutube} from "@fortawesome/free-brands-svg-icons";

import s from "./ProfileData.module.css";

const ProfileData = ({profile, isOwner, goToEditMode}) => {
    let links = (website = false, main_link = false) => {
        let link_href = website ? website : main_link ? main_link : false,
            link_text = website ? 'Website' : main_link ? 'Main Link' : false;

        if (website || main_link) {
            return (
                <div className={`${s.description_content_item} ${s.description_content_item__column}`}>
                    <a href={link_href}>{link_text}</a>
                </div>
            );
        }

        return false;
    };

    let socialsList = (socials) => {
        if (socials.facebook || socials.vk || socials.twitter || socials.instagram || socials.youtube || socials.github) {
            return (
                <div className={`${s.description_content_item} ${s.description_content_item__column}`}>
                    <span>Socials links: </span>

                    <ul className={`${s.socials_list}`}>
                        { !socials.facebook ? false : <li><a href={socials.facebook} rel="noopener noreferrer" target="_blank"> <FontAwesomeIcon icon={ faFacebookF } color={'#fff'} /> </a></li> }
                        { !socials.vk ? false : <li><a href={socials.vk} rel="noopener noreferrer" target="_blank"> <FontAwesomeIcon icon={ faVk } color={'#fff'} /> </a></li> }
                        { !socials.twitter ? false : <li><a href={socials.twitter} rel="noopener noreferrer" target="_blank"> <FontAwesomeIcon icon={ faTwitter } color={'#fff'} /> </a></li> }
                        { !socials.instagram ? false : <li><a href={socials.instagram} rel="noopener noreferrer" target="_blank"> <FontAwesomeIcon icon={ faInstagram } color={'#fff'} /> </a></li> }
                        { !socials.youtube ? false : <li><a href={socials.youtube} rel="noopener noreferrer" target="_blank"> <FontAwesomeIcon icon={ faYoutube } color={'#fff'} /> </a></li> }
                        { !socials.github ? false : <li><a href={socials.github} rel="noopener noreferrer" target="_blank"> <FontAwesomeIcon icon={ faGithub } color={'#fff'} /> </a></li> }
                    </ul>
                </div>
            )
        }

        return false;
    };

    return (
        <div className={s.description_content}>
            { isOwner &&
            <div className={s.description_content_item}>
                <button onClick={goToEditMode} className="btn priority priority--small">Edit</button>
            </div>
            }

            { profile.aboutMe &&
            <div className={s.description_content_item}>
                <span>About me: </span>

                <p>{profile.aboutMe}</p>
            </div>
            }


            {socialsList(profile.contacts)}

            {links(profile.contacts.website)}

            {links(false, profile.contacts.mainLink)}

            <div className={`${s.description_content_item} ${s.description_content_item__row}`}>
                <span>Looking for a job: </span>

                <span>{ profile.lookingForAJob ? 'Yes' : 'No' }</span>
            </div>

            { profile.lookingForAJob && profile.lookingForAJobDescription &&
            <div className={`${s.description_content_item} ${s.description_content_item__row}`}>
                <span>My professional skills: </span>

                <span>{profile.lookingForAJobDescription}</span>
            </div>
            }
        </div>
    );
};

export default ProfileData;
