import React, {useEffect, useState} from 'react';

import s from './ProfileDescription.module.css';

import avaUndefined from "../../../assets/images/avatar-undefined.jpg";
import ProfileStatusWithHooks from "./ProfileStatus/ProfileStatusWithHooks";
import ProfileDataForm from "./ProfileDataForm/ProfileDataForm";
import ProfileData from "./ProfileData/ProfileData";

const ProfileDescription = ({profile, isUpdateProfile, status, updateStatus, myProfileId, isOwner, saveProfilePhoto, saveProfile}) => {
    let [editMode, setEditMode] = useState(isUpdateProfile);

    useEffect(() => {
        setEditMode(isUpdateProfile);
    }, [isUpdateProfile]);

    const onMainPhotoSelected = (e) => {
        if (e.target.files.length) {
            saveProfilePhoto(e.target.files[0]);
        }
    };

    const onSubmit = formData => {
        saveProfile(formData);

        // if (isUpdateProfile) setEditMode(false);
    };

    return (
        <div>
            { profile.fullName &&
                <h1>{profile.fullName}</h1>
            }

            <img src={!profile.photos.large ? avaUndefined : profile.photos.large} alt={profile.fullName} title={profile.fullName} className={s.profile_image} />

            { isOwner &&
                <div className={s.description_content_item}>
                    <span>Update profile photo: </span>

                    <input type="file" onChange={onMainPhotoSelected}/>
                </div>
            }

            <div className={s.description_content_item}>
                <ProfileStatusWithHooks status={status} updateStatus={updateStatus} profileId={profile.userId} myProfileId={myProfileId} />
            </div>

            { !editMode
                ? <ProfileData profile={profile} isOwner={isOwner} goToEditMode={() => setEditMode(true)}/>
                : <ProfileDataForm initialValues={profile} isOwner={isOwner} onSubmit={onSubmit}/>
            }
        </div>
    );
};

export default ProfileDescription;