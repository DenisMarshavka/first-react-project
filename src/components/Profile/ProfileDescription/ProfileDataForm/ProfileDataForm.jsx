import React from "react";
import s from "../ProfileDescription.module.css";
import {reduxForm} from "redux-form";
import {createField, Input, Textarea} from "../../../../specifications/FormsControlrs/FormsControlrs";

const ProfileDataForm = ({handleSubmit, error, initialValues}) => {
    return (
        <form action='/' onSubmit={handleSubmit} className={`${s.description_content} ${s.description_content__form}`}>
            <button className="btn priority priority--small">Save</button>

            { error &&
                <div className="form_summary_error">
                    {error}
                </div>
            }

            <div className={s.description_content_item}>
                <label htmlFor="full_name">Full name: </label>

                {createField('Enter new Full Name', 'fullName', [], Input, {type: 'text', id: 'full_name'})}
            </div>

            <div className={s.description_content_item}>
                <label htmlFor="about_me">About me: </label>

                {createField('Enter info with about your :)', 'aboutMe', [], Input, {type: 'text', id: 'about_me'})}
            </div>

            <div className={`${s.description_content_item} ${s.description_content_item__row}`}>
                <label htmlFor="looking_job">Looking for a job: </label>

                {createField(null, 'lookingForAJob', [], Input, {type: 'checkbox', id: 'looking_job'})}
            </div>

            <div className={`${s.description_content_item}`}>
                <label htmlFor="professional_skills">My professional skills: </label>

                {createField('Enter skills', 'lookingForAJobDescription', [], Textarea, {type: 'textarea', id: 'professional_skills'})}
            </div>

            <div className={s.description_content_item}>
                <span>Contacts: </span>

                <ul className={s.socials_list_form}>
                    {
                        Object.keys(initialValues.contacts).map(key => {
                            return (
                                <li key={key}>
                                    <label htmlFor={key}>{key} -> </label>

                                    {createField(`Enter your link of the ${key}`, `contacts.${key}`, [], Input, {type: 'text', id: key})}
                                </li>
                            );
                        })
                    }
                </ul>
            </div>
        </form>
    );
};

const ProfileDataReduxForm = reduxForm({
    form: 'editProfile'
})(ProfileDataForm);

export default ProfileDataReduxForm;
