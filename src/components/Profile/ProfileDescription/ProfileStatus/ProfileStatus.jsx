import React from "react";

import s from './ProfileStatus.module.css';

class ProfileStatus extends React.Component {
    state = {
        editMode: false,
        status: this.props.status,
        approveToEdit: false
    };

    toggleEditMode(status) {
        this.setState({
            approveToEdit: this.props.profileId === this.props.myProfileId
        });

        if (this.state.approveToEdit) {
            this.setState({
                editMode: status
            });

            if (!status) this.props.updateStatus(this.state.status);
        }
    };

    onStatusChange = (e) => {
        this.setState({
            status: e.currentTarget.value
        });
    };

    componentDidUpdate(prevProps) {
        if (prevProps.status !== this.props.status) {
            this.setState({
                status: this.props.status
            });
        }
    };

    render() {
        return (
            <div>
                Profile Status:

                {!this.state.editMode &&
                    <div>
                        <span onDoubleClick={ this.toggleEditMode.bind(this, true) }>{ this.props.status || "--Is empty--"}</span>
                    </div>
                }

                {this.state.editMode &&
                    <div>
                        <input type="text" autoFocus={true} onBlur={ this.toggleEditMode.bind(this, false) } value={this.state.status} onChange={this.onStatusChange} />
                    </div>
                }
            </div>
        );
    }
}

export default ProfileStatus;