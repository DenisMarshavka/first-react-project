import React from "react";
import ProfileStatus from "./ProfileStatus";
import {create} from "react-test-renderer";
import SamuraiJsApp from "../../../../App";
import * as ReactDOM from "react-dom";

describe('ProfileStatus component', () => {
    test('Renders without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(<SamuraiJsApp />, div);
        ReactDOM.unmountComponentAtNode(div);
    });

    test('After creation span with status should be displayed', () => {
        const component = create(<ProfileStatus status="it kamasutra" />);
        const instance = component.root;
        let span = instance.findByType('span');
        expect(span).not.toBeNull();
    });

    test('After creation span with status should`t be displayed', () => {
        const component = create(<ProfileStatus status="it kamasutra" />);
        const instance = component.root;

        expect(() => {
            instance.findByType('input');
        }).toThrow();
    });

    test('After creation span with status should be contain correct data', () => {
        const component = create(<ProfileStatus status="it kamasutra" />);
        const instance = component.root;
        let span = instance.findByType('span');
        expect(span.children[0]).toBe('it kamasutra');
    });

    test('Input with correct status, should be displayed if activated edit mode', () => {
        const component = create(<ProfileStatus status="it kamasutra" />);
        const instance = component.root;

        let span = instance.findByType('span');
        span.props.onDoubleClick();

        let input = instance.findByType('input');
        expect(input.props.value).toBe('it kamasutra');
    });

    test('Span should`t be displayed if activated edit mode', () => {
        const component = create(<ProfileStatus status="it kamasutra" />);
        const instance = component.root;

        let span = instance.findByType('span');
        span.props.onDoubleClick();

        expect(() => {
            instance.findByType('span');
        }).toThrow();
    });

    test('Callback should be called', () => {
        const mockCallBack = jest.fn();

        const component = create(<ProfileStatus status="it kamasutra" updateStatus={mockCallBack} />);
        const instance = component.getInstance();

        instance.toggleEditMode(false);

        expect(mockCallBack.mock.calls.length).toBe(1);
    });
});