import React, {useEffect, useState} from "react";

const ProfileStatusWithHooks = props => {
    let [editMode, setEditMode] = useState(false);
    let [statusApprove, setStatusApprove] = useState(false);
    let [statusProfile, setStatus] = useState(props.status);

    useEffect(() => {
        setStatus(props.status);
    }, [props.status]);

    const toggleEditMode = (status) => {
        setStatusApprove(props.profileId === props.myProfileId);

        if (statusApprove) {
            setEditMode(status);

            if (!status) props.updateStatus(statusProfile);
        }
    };

    const onStatusChange = (e) => {
        setStatus(e.currentTarget.value);
    };

    return (
        <>
            <span>Profile Status: </span>

            { !editMode &&
                <div>
                    <span onDoubleClick={ () => {toggleEditMode(true)} }>{ props.status || "--Is empty--"}</span>
                </div>
            }

            { editMode &&
                <div>
                    <input type="text" autoFocus={true} onChange={onStatusChange} onBlur={ () => {toggleEditMode(false)} } value={statusProfile} />
                </div>
            }
        </>
    );
};

export default ProfileStatusWithHooks;