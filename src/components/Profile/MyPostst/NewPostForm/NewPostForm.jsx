import React from "react";

import s from './NewPorstForm.module.css';
import {Field, reduxForm} from "redux-form";
import {maxLengthCreator, required} from "../../../../utils/validators/validators";
import {Textarea} from "../../../../specifications/FormsControlrs/FormsControlrs";

const maxLength10 = maxLengthCreator(10);

const NewPostForm = props => {
    return (
        <form action="/" className={s.form_add} onSubmit={props.handleSubmit}>
            <Field name="new_post" id="js-new-post" cols="30" rows="5" placeholder="New post text" component={Textarea} validate={[required, maxLength10]} />

            <button>Add post</button>
        </form>
    );
};

export default reduxForm({form: 'add_post'})(NewPostForm);