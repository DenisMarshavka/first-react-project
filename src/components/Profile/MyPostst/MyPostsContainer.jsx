import React from 'react';
import {connect} from "react-redux";

import {addPostActionCreator} from "../../../redux/profile-reducer";
import MyPosts from "./MyPosts";

class MyPostsContainer extends React.Component {
    onAddPost = values => {
        this.props.addPost(values.new_post);
    };

    render() {
        return <MyPosts onAddPost={this.onAddPost} posts={this.props.posts} />;
    }
}

let mapStateToProps = state => {
    return {
        posts: state.profilePage.posts,
        newText: state.profilePage.newPostText
    }
};

let mapDispatchToProps = dispatch => {
    return {
        addPost: newPostText => {
            dispatch(addPostActionCreator(newPostText));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MyPostsContainer);