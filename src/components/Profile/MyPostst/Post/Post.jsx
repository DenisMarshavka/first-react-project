import React from 'react';

import Ava1 from "../../../../assets/images/Ava1.jpg";

import classes from './Post.module.css';

const Post = props => {
    return (
        <div className={classes.item}>
            <img src={Ava1} alt="Ava for the post 1"/>

            <div className={classes.content}>
                <span> {props.message} </span>

                <div className={classes.system_like}>
                    <button>like</button>

                    <span> { props.countLike } </span>
                </div>
            </div>
        </div>
    );
}

export default Post;