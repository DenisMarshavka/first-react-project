import React from 'react';

import classes from './MyPosts.module.css';
import Post from "./Post/Post";
import NewPostForm from "./NewPostForm/NewPostForm";

const MyPosts = React.memo(props => {
    let PostElements = props.posts.map(post => <Post key={post.id} message={post.message} ountLike={post.countLike}/>);

    return (
        <div>
            <h3 className={classes.title}>My posts</h3>

            <NewPostForm onSubmit={props.onAddPost}/>

            <div className={classes.posts}>
                {PostElements}
            </div>
        </div>
    );
});

export default MyPosts;