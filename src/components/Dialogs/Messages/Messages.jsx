import React from 'react';

import s from './Messages.module.css';
import MessageForm from "./MessageForm/MessageForm";

const Messages = props => {
    let messages = props.messages.map( message => <MessageItem key={message.id} message={message.text} /> );

    return (
        <div className={s.messages}>
            { messages }

            <MessageForm onSubmit={props.onAddMessageNew} />
        </div>
    );
};

const MessageItem = props => {
    return <div className={s.message}> { props.message } </div>;
};


export default Messages;