import React from "react";

import s from "./MessageForm.module.css";
import {Field, reduxForm} from "redux-form";
import {Textarea} from "../../../../specifications/FormsControlrs/FormsControlrs";
import {maxLengthCreator, required} from "../../../../utils/validators/validators";

const maxLength50 = maxLengthCreator(50);

const MessageForm = props => {
    return (
        <form action="/" className={s.new_message} onSubmit={props.handleSubmit}>
            <Field name="add_message" id="add-message" placeholder="Enter your message" component={Textarea} validate={[required, maxLength50]} />

            <button>Send</button>
        </form>
    );
};

export default reduxForm({form: 'new_message'})(MessageForm);