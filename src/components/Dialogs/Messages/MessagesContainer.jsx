import React from 'react';
import {connect} from "react-redux";

import {addMessageActionCreator} from "../../../redux/dialogs-reducer";
import Messages from "./Messages";
import {getMessages, getNewMessageBody} from "../../../redux/selectors/messages-selectors";

class MessagesContainer extends React.Component {
    onAddMessageNew = values => {
        this.props.addMessage(values.add_message);
    };

    render() {
        return <Messages messages={this.props.messages} onAddMessageNew={this.onAddMessageNew} />;
    }
}

let mapStateToProps = state => ({
    messages: getMessages(state),
    newMessageBody: getNewMessageBody(state)
});

let mapDispatchToProps = dispatch => {
    return {
        addMessage: newMessageText => {
            dispatch(addMessageActionCreator(newMessageText));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MessagesContainer);