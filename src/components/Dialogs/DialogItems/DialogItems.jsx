import React from 'react';
import {NavLink} from "react-router-dom";

import s from './DialogItems.module.css';

const DialogItems = props => {
    let dialogsElements = props.dialogs.map( dialog => <DialogItem key={dialog.id} name={dialog.name} id={dialog.id} /> );

    return <div className={s.dialogs_list}> { dialogsElements } </div>;
};

const DialogItem = (props) => {
    let path = '/messages/' + props.id;

    return (
        <NavLink to={path} className={`${s.dialog} ${s.active}`}>
            {props.name}
        </NavLink>
    );
};

export default DialogItems;