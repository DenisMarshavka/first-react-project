import {connect} from "react-redux";

import DialogItems from "./DialogItems";
import withAuthRedirectComponent from "../../../hoc/withAuthRedirect";
import {getDialogs} from "../../../redux/selectors/messages-selectors";

let AuthRedirectComponent = withAuthRedirectComponent(DialogItems);

let mapStateToProps = state => ({
    dialogs: getDialogs(state),
});

export default connect(mapStateToProps)(AuthRedirectComponent);
