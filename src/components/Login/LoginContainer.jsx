import React from "react";
import {reduxForm} from "redux-form";
import {connect} from "react-redux";

import s from './Login.module.css';

import LoginForm from "./Login";
import {getLogin} from "../../redux/auth-reducer";
import {Redirect} from "react-router-dom";
import {getAuthStatus, getCaptchaImage} from "../../redux/selectors/auth-selectors";

const LoginReduxForm = reduxForm({
    form: 'login'
})(LoginForm);

class LoginContainer extends React.Component {
    getSubmit = (formData) => {
        this.props.getLogin(formData.email, formData.password, formData.remember, formData.captcha);
    };

    render() {
        if (this.props.isAuth) return <Redirect to="/profile" />;

        return (
            <div className={s.login_wrapper}>
                <h1>Login</h1>

                <LoginReduxForm captchaImageUrl={this.props.captchaImage} onSubmit={this.getSubmit} />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    captchaImage: getCaptchaImage(state),
    isAuth: getAuthStatus(state)
});

export default connect(mapStateToProps, {getLogin})(LoginContainer);