import React from "react";
import s from "./Login.module.css";
import {createField, Input} from "../../specifications/FormsControlrs/FormsControlrs";
import {maxLengthCreator, required, requiredCheckbox} from "../../utils/validators/validators";

const maxLoginLength = maxLengthCreator(35);
const maxPasswordLength = maxLengthCreator(25);
const maxCaptchaLength = maxLengthCreator(20);

const LoginForm = ({handleSubmit, captchaImageUrl, error}) => {
    return (
        <form className={s.form} onSubmit={handleSubmit}>
            <div className={s.form_row}>
                {createField('Your Email', 'email', [required, maxLoginLength], Input, {type: 'text'})}
            </div>

            <div className={s.form_row}>
                {createField('Your password', 'password', [required, maxPasswordLength], Input, {type: 'password'})}
            </div>

            <div className={`${s.form_row} ${s.form_row_checkbox}`}>
                {createField(null, 'remember', requiredCheckbox, Input, {type: 'checkbox', id: 'remember'})}

                <label htmlFor="remember">Remember me</label>
            </div>

            { captchaImageUrl &&
                <div className={`${s.form_row} ${s.form_row__captcha}`}>
                    <img src={captchaImageUrl} alt="Login captcha" className={s.form_captcha}/>

                    {createField('Enter captcha of the image', 'captcha', [required, maxCaptchaLength], Input, {type: 'text'})}
                </div>
            }

            { error &&
                <div className="form_summary_error">
                    {error}
                </div>
            }

            <div className={s.form_row}>
                <button>Sign In</button>
            </div>
        </form>
    );
};

export default LoginForm;