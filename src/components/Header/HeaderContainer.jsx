import React from 'react';
import {connect} from "react-redux";

import Header from "./Header";
import {logout} from "../../redux/auth-reducer";
import {compose} from "redux";
import {withRouter} from "react-router-dom";
import {getAuthLogin, getAuthStatus, getFetchingStatus} from "../../redux/selectors/auth-selectors";

class HeaderContainer extends React.Component {
    render() {
        return <Header {...this.props} />;
    }
}

let mapStateToProps = state => ({
    isFetching: getFetchingStatus(state),
    isAuth: getAuthStatus(state),
    login: getAuthLogin(state)
});

export default compose(
    withRouter,
    connect(
        mapStateToProps,
        {logout}
    )
)(HeaderContainer);