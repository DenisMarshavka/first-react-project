import React from 'react';
import Logo from "../../assets/images/logo.svg";

import s from './Header.module.css';
import {NavLink} from "react-router-dom";
import Preloader from "../../specifications/Preloader/Preloader";

const Header = props => {
    return (
        <header className={s.header}>
            <a href="/">
                <img src={ Logo } alt="Logo"/>
            </a>

            <div className={s.login_block}>
                { (props.isAuth && props.login && !props.isFetching)
                    ? <span className={s.login_info}>
                        <span>{props.login}</span>

                        <button onClick={props.logout} className={'btn priority'}>Logout</button>
                      </span>
                    : (!props.isAuth && !props.isFetching)
                        ? <NavLink to={'/login'} className={'btn priority'}>Login</NavLink>
                        : <Preloader/> }
            </div>
        </header>
    );
}

export default Header;