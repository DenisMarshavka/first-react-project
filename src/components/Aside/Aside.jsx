import React from 'react';
import {NavLink} from "react-router-dom";

import s from './Aside.module.css';

import FriendsContainer from "./Friends/FriendsContainer";

const Aside = props => {
    return (
        <aside className={s.nav}>
            <nav>
                <ul>
                    <li className={ `${s.item}` }>
                        <NavLink to="/profile">Profile</NavLink>
                    </li>

                    <li className={`${s.item} ${s.item__disabled}`}>
                        <a href="/">News</a>
                    </li>

                    <li className={s.item}>
                        <NavLink to="/messages">Messages</NavLink>
                    </li>

                    <li className={`${s.item} ${s.item__disabled}`}>
                        <a href="/">Music</a>
                    </li>

                    <li className={`${s.item} ${s.item__disabled}`}>
                        <a href="/">Settings</a>
                    </li>

                    <li className={`${s.item} ${s.item__users}`}>
                        <NavLink to="/users">Users</NavLink>
                    </li>
                </ul>
            </nav>

            <FriendsContainer />
        </aside>
    );
};

export default Aside;
