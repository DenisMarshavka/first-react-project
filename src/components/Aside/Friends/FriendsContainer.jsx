import {connect} from "react-redux";

import Friends from "./Friends";
import withAuthRedirectComponent from "../../../hoc/withAuthRedirect";
import {compose} from "redux";
import {getSidebarInfo} from "../../../redux/selectors/sidebar-selectors";

let mapStateToProps = state => {
    return {
        sidebar: getSidebarInfo(state)
    }
};

export default compose(
    connect(mapStateToProps),
    withAuthRedirectComponent
)(Friends);
