import React from 'react';
import {NavLink} from "react-router-dom";

import s from './Friends.module.css';

const Friends = props => {
    let friends = props.sidebar.friends
        .map(friend => (
                <li key={friend.id}>
                    <NavLink to={`/profile/${friend.id}`}>
                        <img className={s.ava} src={friend.img} alt={friend.altImg}/>

                        <span className={s.title_profile}> {friend.profileName} </span>
                    </NavLink>
                </li>
            )
        );

    return (
        <div className={s.friends_box}>
            <h4 className="title">Friends</h4>

            <ul className={s.friends_list}>
                { friends }
            </ul>
        </div>
    );
};

export default Friends;