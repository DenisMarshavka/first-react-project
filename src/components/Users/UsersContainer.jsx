import React, {useEffect} from "react";
import {connect} from "react-redux";
import {
    follow, getUsers,
    unfollow
} from "../../redux/users-reducer";

import Users from "./Users";
import {compose} from "redux";
import {
    getCurrentPage, getFollowingProgress,
    getPageSize,
    getStateUsers,
    getStatusFetching,
    getTotalUsersCount
} from "../../redux/selectors/users-selectors";
import s from "./Users.module.css";

const UsersContainer = (props) => {
    const refUsersList = React.createRef();

    useEffect(() => {
        let {
            totalUsersCount,
            getUsers,
            currentPage,
            pageSize
        } = props;

        if (!totalUsersCount) getUsers(currentPage, pageSize);

    }, []);

    const onPageChanged = () => {
        let {
            pageSize,
            users,
            totalUsersCount
        } = props;

        let pageNumber = Math.ceil(users.length / pageSize) + 1;

        if ( Math.ceil(totalUsersCount / pageSize) !== pageNumber ) {
            props.getUsers(pageNumber, pageSize);
        }
    };

    const handleScroll = () => {
        if ( !props.isFetching && (refUsersList.current.scrollTop + refUsersList.current.clientHeight) >= refUsersList.current.scrollHeight / 2) {
            onPageChanged();
        }
    };

    return (
        <div className={s.users_wrapper} onScroll={handleScroll} ref={refUsersList}>
            <Users
                users={props.users}
                follow={props.follow}
                unfollow={props.unfollow}
                handleScroll={handleScroll}
                isFetching={props.isFetching}
                followingInProgress={props.followingInProgress}
            />
        </div>
    );
};

let mapStateToProps = state => ({
    users: getStateUsers(state),
    pageSize: getPageSize(state),
    totalUsersCount: getTotalUsersCount(state),
    currentPage: getCurrentPage(state),
    isFetching: getStatusFetching(state),
    followingInProgress: getFollowingProgress(state)
});

export default compose(
    connect(mapStateToProps, {follow, unfollow, getUsers})
)(UsersContainer);