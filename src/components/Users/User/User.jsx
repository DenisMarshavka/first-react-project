import React from "react";
import {NavLink} from "react-router-dom";

import s from "./User.module.css";
import avaUndefined from "../../../assets/images/avatar-undefined.jpg";

let User = ({user, followingInProgress, unfollow, follow}) => {
    let userCountryInfo = (city = false, country = false) => {
        if (city && country) {
            return (
                <span className={s.profile_info_live}>
                    {city}, {country}
                </span>
            )
        }

        return false;
    };

    return (
        <div key={user.id} className={s.profile}>
            <div className={s.profile_section_left}>
                <NavLink to={`/profile/${user.id}`}>
                    <img src={ (user.photos.small) ? user.photos.small : avaUndefined } alt={`Avatar of user ${user.name}`} title={user.name}/>
                </NavLink>

                {
                    (user.followed) ?
                        <button className="btn" disabled={followingInProgress.some(id => id === user.id)} onClick={ () => unfollow(user.id) }>Unfollow</button>
                        :
                        <button className="btn" disabled={followingInProgress.some(id => id === user.id)} onClick={ () => follow(user.id) }>Follow</button>
                }
            </div>


            <div className={s.profile_section_right}>
                <div className={s.profile_info}>
            <span>
                {user.name}
            </span>

                    <p className={s.profile_status}>
                        {user.status}
                    </p>
                </div>


                { userCountryInfo( (user.location && user.location.city) ? user.location.city : false, (user.location && user.location.country) ? user.location.country : false) }
            </div>
        </div>
    );
};

export default User;