import React from "react";

import Preloader from "../../specifications/Preloader/Preloader";
import User from "./User/User";

class Users extends React.Component {
    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return nextProps.users !== this.props.users || this.props.followingInProgress !== nextProps.followingInProgress;
    }

    render() {
        let {users, followingInProgress, unfollow, follow, isFetching} = this.props;
        let ListUsers = users.map(u => <User key={u.id} user={u} followingInProgress={followingInProgress} unfollow={unfollow} follow={follow}/>);

        return (
            <>
                {(!isFetching) ? ListUsers : <Preloader/>}
            </>
        )
    }
}

export default Users;/* React.forwardRef((props, ref) => <Users forwardedRef={ref} {...props}/>);*/