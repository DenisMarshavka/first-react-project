import React from 'react';
import { render } from '@testing-library/react';
import App from './App';
import SamuraiJsApp from './App';
import * as ReactDOM from "react-dom";

test('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<SamuraiJsApp />, div);
  ReactDOM.unmountComponentAtNode(div);
});
