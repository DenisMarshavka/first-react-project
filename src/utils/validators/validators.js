export const required = value => {
    if (value) return undefined;

    return "Field is required";
};

export const requiredCheckbox = (value = false) => {
    return !value;
};

export const maxLengthCreator = maxLength => value => {
    if (value && value.length > maxLength) return `Max length is ${maxLength} symbols`;

    return undefined;
};