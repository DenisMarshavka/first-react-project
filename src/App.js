import React, {Component} from 'react';
import {connect, Provider} from "react-redux";
import {BrowserRouter, Redirect, Route, Switch, withRouter} from "react-router-dom";

import './App.css';

import Aside from "./components/Aside/Aside";
import ProfileContainer from "./components/Profile/ProfileContainer";
import HeaderContainer from "./components/Header/HeaderContainer";
import {initializeApp} from "./redux/app-reducer";
import Preloader from "./specifications/Preloader/Preloader";
import {compose} from "redux";
import store from "./redux/redux-store";
import withSuspense from "./hoc/withSuspense";
import {getInitializedStatus} from "./redux/selectors/app-selectors";

const LoginContainer = React.lazy(() => import("./components/Login/LoginContainer"));
const UsersContainer = React.lazy(() => import("./components/Users/UsersContainer"));
const Dialogs = React.lazy(() => import("./components/Dialogs/Dialogs"));

class App extends Component {
    catchAllUnhandledError = (response, promise) => {
        alert("Some error occurred");
    };

    componentDidMount() {
        this.props.initializeApp();

        window.addEventListener("unhandledrejection", this.catchAllUnhandledError);
    }

    componentWillUnmount() {
        window.removeEventListener("unhandledrejection", this.catchAllUnhandledError);
    }

    render() {
        if (!this.props.initialized) return <Preloader />;

        return (
            <div className="app-wrapper">
                <HeaderContainer/>

                <Aside/>

                <div className="app-wrapper-content">
                    <Switch>
                        <Route path="/profile/:userId?" render={() => <ProfileContainer/>}/>
                        <Route path="/messages" render={withSuspense(Dialogs)}/>
                        <Route path="/users" render={withSuspense(UsersContainer)}/>
                        <Route path="/login" render={withSuspense(LoginContainer)}/>

                        <Route path="/" exact>
                            <Redirect to="/profile"/>
                        </Route>

                        <Route path="*" render={() => <div className="error_text">404 This page is NOT FOUND</div>}/>
                    </Switch>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    initialized: getInitializedStatus(state)
});

let AppContainer = compose(
    withRouter,
    connect(mapStateToProps, {initializeApp})
)(App);

const SamuraiJsApp = () => {
    return (
        <BrowserRouter>
            <Provider store={store} >
                <AppContainer />
            </Provider>
        </BrowserRouter>
    );
};

export default SamuraiJsApp;
