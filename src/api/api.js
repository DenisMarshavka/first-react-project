import * as axios from "axios";

//pegupegu99@mail.ru

const instance = axios.create({
    withCredentials: true,
    baseURL: 'https://social-network.samuraijs.com/api/1.0/',
    headers: {
        'API-KEY': '6b402d8b-02ff-470f-993c-e5d926244744'
    }
});

export const securityAPI = {
    getCaptcha() {
        return instance.get(`security/get-captcha-url`)
            .then(response => response.data);
    }
};

export const authAPI = {
    checkAuth() {
        return instance.get(`auth/me`)
            .then(response => response.data);
    },

    login(email, password, rememberMe = false, captcha = null) {
        return instance.post(`auth/login`, {email, password, rememberMe, captcha})
            .then(response => response.data);
    },

    logOut() {
        return instance.delete('auth/login')
            .then(response => response.data);
    }
};

export const usersAPI = {
    getUsers(pageNumber = 1, pageSize = 5) {
        return instance.get(`users?page=${pageNumber}&count=${pageSize}`)
            .then(response => response.data);
    }
};

export const profileAPI = {
    getProfile(userId) {
        return instance.get(`profile/${userId}`);
    },

    getStatus(userId) {
        return instance.get(`profile/status/${userId}`)
            .then(response => response.data);
    },

    updateStatus(newStatusText) {
        return instance.put(`profile/status`, {status: newStatusText})
            .then(response => response.data);
    },

    saveProfilePhoto(photoFile) {
        let formData = new FormData();
        formData.append('image', photoFile);

        return instance.put(`profile/photo`, formData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }).then(response => response.data);
    },

    saveProfileData(newData) {
        return instance.put(`profile`, newData)
            .then(response => response.data);
    }
};

export const followAPI = {
    follow(id) {
        return instance.post(`follow/${id}`)
            .then(response => response.data);
    },

    unfollow(id) {
        return instance.delete(`follow/${id}`)
            .then(response => response.data);
    }
};
