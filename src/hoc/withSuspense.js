import React, {Suspense} from "react";
import Preloader from "../specifications/Preloader/Preloader";

const withSuspense = Component => {
    return (props) => {
        return (
            <Suspense fallback={<Preloader/>}>
                <Component {...props}/>
            </Suspense>
            );
    };
};

export default withSuspense;